# Programme de calcul d'altitude
Se programme est basé sur l'API de geoservices.ign.fr, il ne fonctionne que sur des latitudes et longitudes du territoire français. L'utilisation de cette API ne nécessite pas de clé.
La bibliothèque crée est libre d'utilisation. Elle est située dans le dossier `/src`.

Aucune documentation n'est fournie mis à part ce README.

## Utilisation simple
Mettre un fichier `coords.csv` à la racine, le programme crée un autre fichier `altitude.csv` au même niveau. 
### Pré-requis pour le ficher coords.csv
Le fichier `coords.csv` doit être rempli de la manière suivante :

latitude,longitude
45,5.7
46,6.7
44,6.7
43,6

Dans un éditeur de csv cela donne :
|latitude|longitude|
|-|-|
|45|5.7|
|46|6.7|
|44|6.7|
|43|6|

### Fichier altitude produit

**Altitude.csv** :
Altitude
639.03
1679.65
1268.23
-99999.0

|Altitude|
|-|
|639.03|
|1679.65|
|1268.23|
|-99999.0|

Les valeurs d'élévations sont en mètres. Le cas -99999.0 est crée si le point de coordonées n'est pas en France.

## Utilisation avancée

+ TODO
  + Multi files support
  + kml files reading
  + English documentation version 

## License
MIT License

Copyright (c) 2024 Charles Damaggio

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.