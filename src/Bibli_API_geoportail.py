# %%
import requests
import csv

# %%
def read_csv(file_path):
    with open(file_path, mode='r') as file:
        csv_reader = csv.reader(file)
        # Skip the header row
        next(csv_reader)
        data = []
        for row in csv_reader:
            data.append(row)
    return data

# %%
def to_csv(filename,data):
    # Writing to CSV file
    with open(filename, mode='w', newline='') as file:
        writer = csv.writer(file)
        
        # Write the header
        writer.writerow(["Altitude"])
        
        # Write each altitude value as a new row
        for altitude in data:
            writer.writerow([altitude])
    return

# %%
def dms_to_dd(degrees, minutes, seconds):
    return degrees + (minutes / 60) + (seconds / 3600)

def LAT_LON_dms_to_dd(LAdegrees, LAminutes, LAseconds, LOdegrees, LOminutes, LOseconds):
    return [dms_to_dd(LAdegrees, LAminutes, LAseconds),dms_to_dd(LOdegrees, LOminutes, LOseconds)]

# %%
def from_list_lat_list_lon_make_list_couples(listLAT :list,listLON :list):
    if len(listLAT) != len(listLON):
        print("Liste de points de tailles différentes")
        return
    liste_couples = []
    for i in range(len(listLAT)):
        liste_couples.append([listLAT[i],listLON[i]])
    return liste_couples

# %%
def make_couple_lat_lon(latitude,longitude):
    return ([latitude,longitude])

def get_url_altitude_from_couples_lat_lon(list_lat_lon):
    lat = ""
    lon = ""
    for elements in list_lat_lon:
        if lat != "" and lon != "":
            lat +="|"
            lon +="|"
        lat += str(elements[0])
        lon += str(elements[1])
    return f'https://wxs.ign.fr/calcul/alti/rest/elevation.json?lon={lon}&lat={lat}&zonly=true'


# %%
def get_data_from_csv_coordonate_file(file_source,file_destination):
    file_path = file_source
    Fdata = read_csv(file_path)
    url = get_url_altitude_from_couples_lat_lon(Fdata)
    response = requests.get(url)
    data = response.json()
    if 'elevations' in data and len(data['elevations']) > 0:
        altitude = data['elevations']
        print(f'The altitude is stored in the "altitude.csv" file')
        to_csv("altitude.csv",altitude)
        
    else:
        print("Elevation data not found.")
